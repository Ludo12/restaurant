<?php

namespace App\Controller;

use App\Entity\Avis;
use App\Entity\Client;
use App\Entity\Menu;
use App\Entity\Plat;
use App\Entity\User;
use App\Form\AvisType;
use App\Repository\PlanningRepository;
use App\Repository\MenuRepository;
use App\Repository\PlatRepository;
use DateTime;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     * @param MenuRepository $repo
     * @param PlanningRepository $planningRepository
     * @return Response
     */
    public function home(MenuRepository $repo, PlanningRepository $planningRepository)
    {
        $planningsMidi = $planningRepository->findByService('midi');
        $planningsSoir = $planningRepository->findByService('soir');
        $menus = $repo->findAll();
        return $this->render('home/index.html.twig', [
            'menu' => 'actif',
            'bg' => 'bg-img-home',
            'menus' => $menus,
            'planningM' => $planningsMidi,
            'planningS' => $planningsSoir
        ]);
    }

    /**
     * @Route("/menu/{slug}-{id}", name="home.show", requirements={"slug":"[a-z0-9\-]*"})
     * @param Menu $menu
     * @return Response
     */
    public function show_menu(Menu $menu)
    {
        return $this->render('home/menu/show.html.twig', [
            'menu' => $menu
        ]);
    }

    /**
     * @Route("/plat", name="plat.home")
     * @param PlatRepository $repo
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function show(PlatRepository $repo, PaginatorInterface $paginator, Request $request)
    {
        $plat = $paginator->paginate($repo->findAll(),
            $request->query->getInt('page', 1), /*page number*/
            12 /*limit per page*/
        );


        return $this->render('home/plat/home.html.twig', [
            'menu' => 'actif',
            'bg' => 'bg-img-plat',
            'plats' => $plat,
        ]);
    }

    /**
     * @Route("/plat/{slug}-{id}", name="plat.show", requirements={"slug":"[a-z0-9\-]*"})
     * @param Plat $plat
     * @param Request $request
     * @param User $user
     * @return Response
     * @throws Exception
     */
    public function show_plat(Plat $plat, Request $request, User $user)
    {
        $list_avis = $this->getDoctrine()->getRepository(Avis::class)->last_avis();
        $avis = new Avis();
        $form = $this->createForm(AvisType::class, $avis);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->IsGranted($user)) {
            $client = $this->getDoctrine()->getRepository(Client::class)->find($user);
            $avis->setCreatedAt(new DateTime())
                ->setPlats($plat)
                ->setClient($client);

            $em = $this->getDoctrine()->getManager();
            $em->persist($avis);
            $em->flush();

            return $this->redirectToRoute('plat.show', ['id' => $plat->getId()]);
        }

        return $this->render('home/plat/show.html.twig', [
            'list_avis' => $list_avis,
            'plat' => $plat,
            'formAvis' => $form->createView()
        ]);
    }
}

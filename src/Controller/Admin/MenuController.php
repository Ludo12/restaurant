<?php

namespace App\Controller\Admin;

use App\Entity\Menu;
use App\Form\MenuType;
use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin.")
 */
class MenuController extends AbstractController
{
    /**
     * @Route("/menu", name="menu.index")
     * @param MenuRepository $repository
     * @return Response
     */
    public function index(MenuRepository $repository)
    {
        $menus = $repository->findall();
        return $this->render('admin/menu/index.html.twig', ['menus' => $menus]);
    }

    /**
     * @Route("/menu/new", name="menu.new")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request)
    {
        $menu = new Menu();

        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($menu);
            $em->flush();
            $this->addFlash('success', 'Menu créé avec succès');
            return $this->redirectToRoute('admin.menu.index');
        }

        return $this->render('admin/menu/new.html.twig', [
            'formMenu' => $form->createView()
        ]);
    }

    /**
     * @Route("/menu/edit/{id}", name="menu.edit", methods="GET|POST")
     * @param Menu $menu
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(Menu $menu, Request $request)
    {

        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('success', 'Menu modifié avec succès');
            return $this->redirectToRoute('admin.menu.index');
        }
        return $this->render('admin/menu/edit.html.twig', [
            'menu' => $menu,
            'formMenu' => $form->createView()
        ]);
    }

    /**
     * @Route("/menu/{id}", name="menu.delete", methods="DELETE")
     * @param Menu $menu
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Menu $menu, Request $request)
    {
        if ($this->isCsrfTokenValid('delete_menu' . $menu->getId(), $request->get('_token'))) {
            $this->getDoctrine()->getManager()->remove($menu);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Menu supprimé avec succès');
        }
        return $this->redirectToRoute('admin.menu.index');
    }
}

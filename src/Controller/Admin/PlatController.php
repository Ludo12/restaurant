<?php

namespace App\Controller\Admin;

use App\Entity\Plat;
use App\Form\PlatType;
use App\Repository\PlatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin.")
 */
class PlatController extends AbstractController
{

    /**
     * @Route("/plat/", name="plat.index")
     * @param PlatRepository $repository
     * @return Response
     */
    public function index_plat(PlatRepository $repository)
    {
        $plats = $repository->findall();
        return $this->render('admin/plat/index.html.twig', ['plats' => $plats,]);
    }

    /**
     * @Route("/plat/new", name="plat.new")
     * @param Request $request
     * @return Response
     */
    public function new_plat(Request $request)
    {
        $plat = new Plat();

        $form = $this->createForm(PlatType::class, $plat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($plat);
            $em->flush();
            $this->addFlash('success', 'Plat créé avec succès');
            return $this->redirectToRoute('admin.plat.index');
        }

        return $this->render('admin/plat/new.html.twig', [
            'formPlat' => $form->createView(),
        ]);
    }

    /**
     * @Route("/plat/edit/{id}", name="plat.edit", methods="GET|POST")
     * @param Plat $plat
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit_plat(Plat $plat, Request $request)
    {

        $form = $this->createForm(PlatType::class, $plat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('success', 'Plat modifié avec succès');
            return $this->redirectToRoute('admin.plat.index');
        }
        return $this->render('admin/plat/edit.html.twig', [
            'plat' => $plat,
            'formPlat' => $form->createView(),
        ]);
    }

    /**
     * @Route("/plat/{id}", name="plat.delete", methods="DELETE")
     * @param Plat $plat
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete_plat(Plat $plat, Request $request)
    {
        if ($this->isCsrfTokenValid('delete_plat' . $plat->getId(), $request->get('_token'))) {
            $this->getDoctrine()->getManager()->remove($plat);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Plat supprimé avec succès');
        }
        return $this->redirectToRoute('admin.plat.index');
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use App\Entity\PlaceResto;
use App\Form\SearchType;
use App\Repository\ClientRepository;
use App\Repository\ContactRepository;
use App\Repository\PlaceRestoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin.")
 */
class AdminController extends AbstractController
{
    /*********************************
     *  Accueil de l'administration  *
     *********************************/

    /**
     * @Route("/", name="start")
     * @param ClientRepository $clientRepository
     * @param Request $request
     * @return Response
     */
    public function start(ClientRepository $clientRepository, Request $request)
    {
        $search = new Client();
        $form = $this->createForm(SearchType::class, $search);
        $form->handleRequest($request);

        $clients = $clientRepository->findBy(array('name' => $search->getName()));

        if ($form->isSubmitted() && $form->isValid()) {
            if ($clients == null) {
                $this->addFlash('danger', 'Ce nom est inconnu');
                return $this->redirectToRoute('admin.start');
            }
        }

        return $this->render('admin/start.html.twig', [
            'clients' => $clients,
            'form_c' => $form->createView()
        ]);
    }

    /*********************************
     *        les réservations       *
     *********************************/

    /**
     * @Route("/place", name="home")
     * @param PlaceRestoRepository $repo
     * @return Response
     */
    public function home(PlaceRestoRepository $repo)
    {
        $places = $repo->sumByDate();
        return $this->render('admin/place/home.html.twig', ['places' => $places]);
    }

    /**
     * @Route("/place/annulation", name="place.annulation")
     * @param PlaceRestoRepository $repository
     * @return Response
     */
    public function annu_place(PlaceRestoRepository $repository)
    {
        $delete_reservation = $repository->findall();
        return $this->render('admin/place/annul.html.twig', [
            'delete_reservation' => $delete_reservation,]);
    }

    /**
     * @Route("/place/detail", name="detail")
     * @param PlaceRestoRepository $repo
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function place(PlaceRestoRepository $repo, ClientRepository $clientRepository)
    {

        $elements = $repo->all_elements();
        $tab = [];

        foreach ($elements as $p) {
            $tab[$p->getId()] = $clientRepository->trouve($p->getId(), $p->getDateReserv());
        }
        return $this->render('admin/place/index.html.twig', [
            'elements' => $elements,
            'tab' => $tab,
        ]);
    }

    /**
     * @Route("/place/annulation/{id}", name="place.delete", methods="DELETE")
     * @param PlaceResto $placeResto
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete_plat(PlaceResto $placeResto, Request $request)
    {
        if ($this->isCsrfTokenValid('delete_place' . $placeResto->getId(), $request->get('_token'))) {
            $this->getDoctrine()->getManager()->remove($placeResto);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Place annulée avec succès');
        }
        return $this->redirectToRoute('admin.place.annulation');
    }
}

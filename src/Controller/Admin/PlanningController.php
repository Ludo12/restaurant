<?php

namespace App\Controller\Admin;

use App\Entity\Planning;
use App\Form\PlanningType;
use App\Repository\PlanningRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin.")
 */
class PlanningController extends AbstractController
{
    /**
     * @Route("/planning", name="planning.index")
     * @param PlanningRepository $repository
     * @return Response
     */
    public function index(PlanningRepository $repository)
    {
        $plannings = $repository->all_planning();
        return $this->render('admin/planning/index.html.twig', ['plannings' => $plannings]);
    }

    /**
     * @Route("/planning/new", name="planning.new")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request)
    {
        $planning = new Planning();

        $form = $this->createForm(PlanningType::class, $planning);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($planning);
            $em->flush();
            $this->addFlash('success', 'Planning créé avec succès');
            return $this->redirectToRoute('admin.planning.index');
        }

        return $this->render('admin/planning/new.html.twig', [
            'formPlanning' => $form->createView()
        ]);
    }

    /**
     * @Route("/planning/edit/{id}", name="planning.edit", methods="GET|POST")
     * @param Planning $planning
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(Planning $planning, Request $request)
    {

        $form = $this->createForm(PlanningType::class, $planning);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('success', 'Planning modifié avec succès');
            return $this->redirectToRoute('admin.planning.index');
        }
        return $this->render('admin/planning/edit.html.twig', [
            'planning' => $planning,
            'formPlanning' => $form->createView()
        ]);
    }

    /**
     * @Route("/planning/{id}", name="planning.delete", methods="DELETE")
     * @param Planning $planning
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Planning $planning, Request $request)
    {
        if ($this->isCsrfTokenValid('delete_planning' . $planning->getId(), $request->get('_token'))) {
            $this->getDoctrine()->getManager()->remove($planning);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Planning supprimé avec succès');
        }
        return $this->redirectToRoute('admin.planning.index');
    }
}

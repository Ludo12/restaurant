<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin.")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/user/", name="user.index")
     * @param UserRepository $repository
     * @return Response
     */
    public function index(UserRepository $repository)
    {

        return $this->render('admin/user/index.html.twig', [
            'list_client_user' => $repository->findAll()

        ]);
    }

    /**
     * @Route("/user/edit/{id}", name="user.edit", methods="GET|POST")
     * @param User $u_user
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(User $u_user, Request $request)
    {

        $form_u = $this->createForm(UserType::class, $u_user);
        $form_u->handleRequest($request);
        if ($form_u->isSubmitted() && $form_u->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($u_user);
            $em->flush();
            $this->addFlash('success', 'User modifié avec succès');
            return $this->redirectToRoute('admin.user.index');
        }
        return $this->render('admin/user/edit.html.twig', [
            'user' => $u_user,
            'formUser' => $form_u->createView()
        ]);
    }

    /**
     * @Route("/user/{id}", name="user.delete", methods="DELETE")
     * @param client $client
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Client $client, Request $request)
    {
        if ($this->isCsrfTokenValid('delete_user' . $client->getId(), $request->get('_token'))) {
            $this->getDoctrine()->getManager()->remove($client);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'User supprimé avec succès');
        }
        return $this->redirectToRoute('admin.user.index');
    }
}

<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Planning;
use App\Entity\PlaceResto;
use App\Entity\User;
use App\Form\ContactType;
use App\Form\PlaceRestoType;
use App\Notification\ContactNotification;
use App\Repository\PlanningRepository;
use App\Repository\PlaceRestoRepository;
use DateTime;
use DateTimeZone;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile.home")
     * @return Response
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $monId = $this->getUser()->getId();

        $client = $this->getDoctrine()->getRepository(User::class)->find($monId)->getClients();
        $user = $this->getUser();

        return $this->render('profile/index.html.twig', [
            'client' => $client,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/profile/reservation", name="profile.reserv")
     * @param Request $request
     * @param PlaceRestoRepository $repo
     * @param PlanningRepository $planningRepository
     * @return Response
     * @throws Exception
     */
    public function verifplace(Request $request, PlaceRestoRepository $repo, PlanningRepository $planningRepository)
    {
        $plannings = $planningRepository->findAll();

        $placeresto = new PlaceResto();
        $form = $this->createForm(PlaceRestoType::class, $placeresto);
        $form->handleRequest($request);
        $date_now = (new DateTime())->setTimezone(new DateTimeZone('Europe/Paris'));

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $day = $data->getDateReserv();

            $h = $day->format('H');
            $m = $day->format('i');
            $id_h = $planningRepository->findOneBy(array('hour' => $h, 'minute' => $m));

            $service = $planningRepository->find($id_h)->getService();

            $new_day = new DateTime($day->format('Y-m-d') . " 0:0");

            if ($day < $date_now) {
                $this->addFlash('danger', 'La date ou l\'heure choisie n\'est pas bonne');
                return $this->redirectToRoute('profile.reserv', [
                    'form' => $form->createView(),
                    'planning' => $plannings
                ]);
            }

            $placeresto_encours = $data->getPlaceReserv();
            $rest = 50 - intval($repo->findByTotalByDate($new_day, $service)) - $placeresto_encours;


            if ($rest >= 0) {
                $_SESSION['jour'] = $data->getDateReserv();
                $_SESSION['place'] = $data->getPlaceReserv();
                $this->addFlash('success', 'Merci de confirmer la réservation');
                return $this->redirectToRoute('profile.confirm');
            }
            $this->addFlash('danger', 'Désolé, il n\'y a plus de place pour cette date');
            return $this->redirectToRoute('profile.reserv', [
                'form' => $form->createView(),
                'planning' => $plannings
            ]);
        }

        return $this->render('profile/reserv.html.twig', [
            'form' => $form->createView(),
            'plannings' => $plannings
        ]);
    }

    /**
     * @Route("/profile/reservation/confirm", name="profile.confirm")
     * @param ContactNotification $notification
     * @param Request $request
     * @param PlanningRepository $planningRepository
     * @param Swift_Mailer $mailer
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function confirm(ContactNotification $notification, Request $request, PlanningRepository $planningRepository, Swift_Mailer $mailer)
    {

        $contact = new Contact();

        $contact->setDate($_SESSION['jour']);
        $contact->setReservation($_SESSION['place']);
        $formContact = $this->createForm(ContactType::class, $contact);
        $formContact->handleRequest($request);
        if ($formContact->isSubmitted() && $formContact->isValid()) {

            $h = $_SESSION['jour']->format('H');
            $m = $_SESSION['jour']->format('i');

            $id_h = $planningRepository->findOneBy(array('hour' => $h, 'minute' => $m));

            $planning = $this->getDoctrine()->getRepository(Planning::class)->find($id_h);
            $user = $this->getUser()->getId();
            $client = $this->getDoctrine()->getRepository(User::class)->find($user)->getClients();

            $placeresto = new PlaceResto();
            $d = $contact->setDate($_SESSION['jour'])->getDate();
            $p = $contact->setReservation($_SESSION['place'])->getReservation();

            $verif = $this->getDoctrine()->getRepository(PlaceResto::class)->verif_existe($d->format('Y-m-d'), $client->getId(), $planning->getService());

            if ($verif) {
                $this->addFlash('danger', 'Vous avez déjà fais une réservation à cette date et/ou pour ce service');
                return $this->redirectToRoute('profile.reserv');
            }

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($contact);

            $date_t = new DateTime($d->format('Y-m-d') . " 0:0");

            $placeresto->setDateReserv($date_t);
            $placeresto->setPlaceReserv($p);
            $placeresto->setClients($client);
            $placeresto->setPlanning($planning);

            $contact->setClient($client);

            $manager->persist($placeresto);
            $manager->flush();
            $notification->notify($contact);
            $message = (new Swift_Message('Réservation'))
                // On attribue l'expéditeur
                ->setFrom($contact->getEmail())
                // On attribue le destinataire
                ->setTo('noreply@restau.fr')
                ->setBody(
                    "<p>Une réservation a été effectuée de la part de " . $contact->getFirstname() . " " . $contact->getLastname() . " pour le " . $contact->getDate()->format('d-m-Y H:i') . "." . "</p><p>" . $contact->getMessage() . "</p>",
                    'text/html'
                );
            $mailer->send($message);
            $this->addFlash('success', 'Votre mail a bien été envoyé');

            return $this->redirectToRoute('app_home');
        }
        return $this->render('profile/confirmation.html.twig', [
            'formContact' => $formContact->createView()
        ]);
    }

    /**
     * @Route("/profile/{id}", name="reservation.delete", methods="DELETE")
     * @param PlaceResto $placeResto
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete_plat(PlaceResto $placeResto, Request $request)
    {
        if ($this->isCsrfTokenValid('delete_reservation' . $placeResto->getId(), $request->get('_token'))) {
            $this->getDoctrine()->getManager()->remove($placeResto);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Réservation annulée avec succès');
        }
        return $this->redirectToRoute('profile.home');
    }
}

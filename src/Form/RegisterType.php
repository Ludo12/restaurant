<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-border-l',
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'first_options' => [
                    'label' => 'Mot de passe',
                    'help' => 'Votre mot de passe doit avoir minimum 8 caractères',
                    'attr' =>
                        ['placeholder' => "Mot de passe",
                            'class' => 'form-border-l',
                        ],

                ],
                'second_options' => [
                    'label' => 'Confirmation Mot de passe',
                    'help' => 'Confirmation du mot de passe',
                    'attr' => [
                        'placeholder' => "Confirmation du mot de passe",
                        'class' => 'form-border-l',
                    ]
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

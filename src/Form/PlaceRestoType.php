<?php

namespace App\Form;

use App\Entity\PlaceResto;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceRestoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('placeReserv', ChoiceType::class, [
                'label_attr' => ['class' => 'd-none'],
                'placeholder' => 'Nombre de personnes',
                'choices' => array(1 => 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
            ])
            ->add('date_reserv', DateTimeType::class, [
                'label_attr' => ['class' => 'd-none'],
                'attr' => ['class' => 'd-none']
            ])
            ->add('Valider', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlaceResto::class,
        ]);
    }
}

<?php

namespace App\Notification;

use App\Entity\Contact;
use Swift_Mailer;
use Swift_Message;
use Twig\Environment;
/**
 *Class permettant l'envoie du mail
 **/
class ContactNotification
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $renderer;

    public function __construct(Swift_Mailer $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function notify(Contact $contact)
    {
        $message = (new Swift_Message('Confirmation de reservation'))
            // On attribue l'expéditeur
            ->setFrom('noreply@restau.fr')
            // On attribue le destinataire
            ->setTo($contact->getEmail())
            // On crée le texte avec la vue
            ->setBody(
                $this->renderer->render('emails/contact.html.twig', [
                    'contact' => $contact
                ]),
                'text/HTML'
            );

        $this->mailer->send($message);
    }
}

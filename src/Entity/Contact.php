<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="2")
     * @Assert\NotBlank()
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="2")
     * @Assert\NotBlank()
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Regex(
     *  pattern="/^((\+)33|0)[1-9](\d{2}){4}$/"
     * )
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Assert\Length(min=10)
     */
    private $message;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Length(max="2")
     */
    private $reservation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="contacts")
     */
    private $client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return htmlspecialchars(strip_tags($this->firstname));
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = htmlspecialchars(strip_tags($firstname));

        return $this;
    }

    public function getLastname(): ?string
    {
        return htmlspecialchars(strip_tags($this->lastname));
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = htmlspecialchars(strip_tags($lastname));

        return $this;
    }

    public function getPhone(): ?string
    {
        return htmlspecialchars(strip_tags($this->phone));
    }

    public function setPhone(string $phone): self
    {
        $this->phone = htmlspecialchars(strip_tags($phone));

        return $this;
    }

    public function getEmail(): ?string
    {
        return htmlspecialchars(strip_tags($this->email));
    }

    public function setEmail(string $email): self
    {
        $this->email = htmlspecialchars(strip_tags($email));

        return $this;
    }

    public function getMessage(): ?string
    {
        return htmlspecialchars(strip_tags($this->message));
    }

    public function setMessage(string $message): self
    {
        $this->message = htmlspecialchars(strip_tags($message));

        return $this;
    }

    public function getReservation(): ?int
    {
        return htmlspecialchars(strip_tags($this->reservation));
    }

    public function setReservation(int $reservation): self
    {
        $this->reservation = htmlspecialchars(strip_tags($reservation));

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="2")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Regex(
     *  pattern="/^((\+)33|0)[1-9](\d{2}){4}$/",
     *     match=true
     * )
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaceResto", mappedBy="clients", cascade={"remove"},orphanRemoval=true)
     */
    private $revplace;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="client",cascade={ "remove"}, orphanRemoval=true)
     */
    private $avis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contact", mappedBy="client", cascade={ "remove"},orphanRemoval=true)
     */
    private $contacts;


    public function __construct()
    {
        $this->revplace = new ArrayCollection();
        $this->avis = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return htmlspecialchars(strip_tags($this->name));
    }

    public function setName(string $name): self
    {
        $this->name = htmlspecialchars(strip_tags($name));

        return $this;
    }

    public function getPhone(): ?string
    {
        return htmlspecialchars(strip_tags($this->phone));
    }

    public function setPhone(string $phone): self
    {
        $this->phone = htmlspecialchars(strip_tags($phone));

        return $this;
    }

    /**
     * @return Collection|PlaceResto[]
     */
    public function getRevplace(): Collection
    {
        return $this->revplace;
    }

    public function addRevplace(PlaceResto $revplace): self
    {
        if (!$this->revplace->contains($revplace)) {
            $this->revplace[] = $revplace;
            $revplace->setClients($this);
        }

        return $this;
    }

    public function removeRevplace(PlaceResto $revplace): self
    {
        if ($this->revplace->contains($revplace)) {
            $this->revplace->removeElement($revplace);
            // set the owning side to null (unless already changed)
            if ($revplace->getClients() === $this) {
                $revplace->setClients(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setClient($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->contains($avi)) {
            $this->avis->removeElement($avi);
            // set the owning side to null (unless already changed)
            if ($avi->getClient() === $this) {
                $avi->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setClient($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getClient() === $this) {
                $contact->setClient(null);
            }
        }

        return $this;
    }

}

<?php

namespace App\Entity;

use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlatRepository")
 * @Vich\Uploadable
 */
class Plat
{
    public function __construct()
    {
        $this->updatedAt = new DateTime();
        $this->avis = new ArrayCollection();
        $this->menus = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="3",minMessage="Le nom est trop court")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="10",minMessage="La description est trop courte")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="10",minMessage="Le text est trop court")
     */
    private $histoire;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="images", fileNameProperty="imageName")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var DateTime|null
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="plats", cascade={ "remove"},orphanRemoval=true)
     */
    private $avis;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Menu", mappedBy="plats")
     */
    private $menus;

    /**
     * @param File|UploadedFile|null $imageFile
     * @throws Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = htmlspecialchars(strip_tags($imageName));
    }

    public function getImageName(): ?string
    {
        return htmlspecialchars(strip_tags($this->imageName));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return htmlspecialchars(strip_tags($this->nom));
    }

    public function setNom(string $nom): self
    {
        $this->nom = htmlspecialchars(strip_tags($nom));

        return $this;
    }

    public function getDescription(): ?string
    {
        return htmlspecialchars(strip_tags($this->description));
    }

    public function setDescription(?string $description): self
    {
        $this->description = htmlspecialchars(strip_tags($description));

        return $this;
    }

    public function getHistoire(): ?string
    {
        return htmlspecialchars(strip_tags($this->histoire));
    }

    public function setHistoire(?string $histoire): self
    {
        $this->histoire = htmlspecialchars(strip_tags($histoire));

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setPlats($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->contains($avi)) {
            $this->avis->removeElement($avi);
            // set the owning side to null (unless already changed)
            if ($avi->getPlats() === $this) {
                $avi->setPlats(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
            $menu->addPlat($this);
        }

        return $this;
    }

    public function removeMenu(Menu $menu): self
    {
        if ($this->menus->contains($menu)) {
            $this->menus->removeElement($menu);
            $menu->removePlat($this);
        }

        return $this;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->nom);
    }
}

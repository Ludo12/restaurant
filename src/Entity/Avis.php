<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvisRepository")
 */
class Avis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $auteur;

    /**
     * @ORM\Column(type="text")
     */
    private $contenu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Plat", inversedBy="avis")
     */
    private $plats;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="avis")
     */
    private $client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getauteur(): ?string
    {
        return htmlspecialchars(strip_tags($this->auteur));
    }

    public function setauteur(string $auteur): self
    {
        $this->auteur = htmlspecialchars(strip_tags($auteur));

        return $this;
    }

    public function getcontenu(): ?string
    {
        return htmlspecialchars(strip_tags($this->contenu));
    }

    public function setcontenu(string $contenu): self
    {
        $this->contenu = htmlspecialchars(strip_tags($contenu));

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPlats(): ?Plat
    {
        return $this->plats;
    }

    public function setPlats(?Plat $plats): self
    {
        $this->plats = $plats;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }
}

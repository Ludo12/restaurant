<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaceRestoRepository")
 */
class PlaceResto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $placeReserv;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_reserv;

    /**
     * @return \DateTime
     */
    public function getDateReserv(): \DateTime
    {
        return $this->date_reserv;
    }

    /**
     * @param \DateTime $date_reserv
     * @return PlaceResto
     */
    public function setDateReserv(\DateTime $date_reserv): PlaceResto
    {
        $this->date_reserv = $date_reserv;
        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="revplace")
     */
    private $clients;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Planning", inversedBy="res")
     */
    private $planning;


    public function __construct()
    {
        $this->date_reserv = new \DateTime();
    }

    public function __toString()
    {
        return $this->date_reserv->format('d-m-Y');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlaceReserv()
    {
        return htmlspecialchars(strip_tags($this->placeReserv));
    }

    public function setPlaceReserv($placeReserv)
    {
        $this->placeReserv = htmlspecialchars(strip_tags($placeReserv));
        return $this;
    }

    public function getClients(): ?Client
    {
        return $this->clients;
    }

    public function setClients(?Client $clients): self
    {
        $this->clients = $clients;

        return $this;
    }

    public function getPlanning(): ?Planning
    {
        return $this->planning;
    }

    public function setPlanning(?Planning $planning): self
    {
        $this->planning = $planning;

        return $this;
    }
}

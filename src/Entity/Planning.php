<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanningRepository")
 */
class Planning
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $hour;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $minute;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $service;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaceResto", mappedBy="planning", orphanRemoval=true)
     */
    private $res;

    public function __construct()
    {
        $this->res = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHour(): ?string
    {
        return htmlspecialchars(strip_tags($this->hour));
    }

    public function setHour(string $hour): self
    {
        $this->hour = htmlspecialchars(strip_tags($hour));

        return $this;
    }

    public function getMinute(): ?string
    {
        return htmlspecialchars(strip_tags($this->minute));
    }

    public function setMinute(string $minute): self
    {
        $this->minute = htmlspecialchars(strip_tags($minute));

        return $this;
    }

    public function getService(): ?string
    {
        return htmlspecialchars(strip_tags($this->service));
    }

    public function setService(string $service): self
    {
        $this->service = htmlspecialchars(strip_tags($service));

        return $this;
    }

    /**
     * @return Collection|PlaceResto[]
     */
    public function getRes(): Collection
    {
        return $this->res;
    }

    public function addRe(PlaceResto $re): self
    {
        if (!$this->res->contains($re)) {
            $this->res[] = $re;
            $re->setPlanning($this);
        }

        return $this;
    }

    public function removeRe(PlaceResto $re): self
    {
        if ($this->res->contains($re)) {
            $this->res->removeElement($re);
            // set the owning side to null (unless already changed)
            if ($re->getPlanning() === $this) {
                $re->setPlanning(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Avis;
use App\Entity\Client;
use App\Entity\Contact;
use App\Entity\Planning;
use App\Entity\Menu;
use App\Entity\PlaceResto;
use App\Entity\Plat;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MenuFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $hor = 11;
        $sor = 18;
        $minu = 0;
        $sminu = 0;
        $service = '';
        $mo = '';
        $ho = '';
        //Créer 3 menus fakées
        for ($m = 1; $m <= 3; $m++) {

            $menu = new Menu();
            $menu->setNom($faker->sentence(3))
                ->setPrix($faker->randomNumber(2));
            $manager->persist($menu);

            //Créer entre 4 et 6 plats et users
            for ($j = 1; $j <= 4; $j++) {
                $plat = new Plat();
                $plat->setNom($faker->sentence())
                    ->setDescription($faker->paragraph(1))
                    ->setHistoire($faker->paragraph(1))
                    ->addMenu($menu);

                $manager->persist($plat);


                $user = new User();
                $client = new Client();
                $user->setEmail($faker->freeEmail)
                    ->setRoles(['ROLE_USER'])
                    ->setPassword($this->passwordEncoder->encodePassword($user, 'azertyui'))
                    ->setClients($client);
                $client->setName($faker->lastName)
                    ->setPhone($faker->phoneNumber);
                $manager->persist($user);
                $manager->persist($client);

                //créer des horraires
                $horraire = new Planning();

                if ($j <= 2 && $hor < 14) {
                    $hor++;
                    $mo = strval($minu);
                    $ho = strval($hor);
                    $minu = $minu + 30;
                    if ($minu == 60) {
                        $minu = 0;
                    }
                    $service = 'midi';

                } elseif ($j > 2 && $sor < 23) {
                    $mo = strval($sminu);
                    $sor++;
                    $ho = strval($sor);
                    $sminu = $minu + 30;
                    if ($sminu == 60) {
                        $sminu = 0;
                    }
                    $service = 'soir';
                }
                if ($mo == "0") {
                    $mo = "00";
                }

                $horraire->setHour($ho)
                    ->setMinute($mo)
                    ->setService($service);
                $manager->persist($horraire);

                for ($p = 0; $p < 5; $p++) {
                    $place = new PlaceResto();
                    $ma_date = $faker->dateTimeBetween('now', '2 weeks')->format('Y-m-d');
                    $place->setPlaceReserv($faker->numberBetween($min = 1, $max = 4))
                        ->setClients($client)
                        ->setPlanning($horraire)
                        ->setDateReserv(new DateTime($ma_date));
                    $manager->persist($place);

                    $contact = new Contact();
                    $the_day = $place->getDateReserv()->format('Y-m-d') . " " . $horraire->getHour() . ":" . $horraire->getMinute();

                    $new_day = new DateTime($the_day);

                    $contact->setFirstname($faker->firstName())
                        ->setLastname($faker->lastName)
                        ->setEmail($faker->freeEmail)
                        ->setPhone($faker->phoneNumber)
                        ->setMessage($faker->paragraph(1))
                        ->setReservation($place->getPlaceReserv())
                        ->setDate($new_day)
                        ->setClient($client);
                    $manager->persist($contact);


                    for ($a = 0; $a < mt_rand(1, 4); $a++) {
                        $avis = new Avis();
                        //On fait un commentaire entre la date du jour et la date de reservation
                        $now = new \DateTime();//date du jour
                        //on récupère la différence entre les 2 objets Datetime
                        $interval = $now->diff($place->getDateReserv());
                        //on récupère le nombre de jour
                        $days = $interval->days;
                        $minim = '+' . $days . 'days'; //example: -100 days

                        $avis->setauteur($faker->firstName)
                            ->setcontenu($faker->paragraph(1))
                            ->setClient($client)
                            ->setPlats($plat)
                            ->setCreatedAt($faker->dateTimeBetween('now', $minim));

                        $manager->persist($avis);
                    }
                }
            }
        }

        $manager->flush();
    }
}

<?php

namespace App\Repository;

use App\Entity\Planning;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Planning|null find($id, $lockMode = null, $lockVersion = null)
 * @method Planning|null findOneBy(array $criteria, array $orderBy = null)
 * @method Planning[]    findAll()
 * @method Planning[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanningRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Planning::class);
    }

    /**
     * @param $value
     * @return Planning[] Returns an array of Planning objects
     */

    public function findByService($value)
    {
        return $this->createQueryBuilder('h')
            ->select('Min(h.hour) as minhour,Max(h.hour) as maxhour')
            ->andWhere('h.service = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }

    public function all_planning()
    {
       return $this->createQueryBuilder('h')
           ->addOrderBy('h.hour')
           ->getQuery()
           ->getResult();
}
    /*
    public function findOneBySomeField($value): ?Planning
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    /**
     * @param $contact
     * @param $place
     * @return Client[] Returns an array of Client objects
     * permet d'obtenir le firstname de contact, la date de la reservation et le nombre de place de placeresto
     * à partir de client en effectuant 2 jointutres
     */

    public function trouve($contact, $place)
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c.name,c.id')
            //On fait une jointure avec l'entité Contact, avec pour alias « co », on joute une condition pour la jointure avec le WITH
            ->join('c.contacts', 'co', 'WITH', 'co.id =:cnt')
            //on attribue la valeur "$contact" au nom du paramètre "cnt"
            ->setParameter('cnt', $contact)
            ->addSelect('co.firstname')
            ->addSelect('co.lastname')
            //On fait une jointure avec l'entité PlaceResto, avec pour alias « r », on joute une condition pour la jointure avec le WITH
            ->join('c.revplace', 'r', 'WITH', 'r.date_reserv =:pla')
            //on attribue la valeur "$place" au nom du paramètre "pla"
            ->setParameter('pla', $place)
            ->addSelect('r.date_reserv')
            ->addSelect('r.placeReserv');

        return $qb->getQuery()->getResult();
    }

    /*
   public function findOneBySomeField($value): ?Client
   {
       return $this->createQueryBuilder('c')
           ->andWhere('c.exampleField = :val')
           ->setParameter('val', $value)
           ->getQuery()
           ->getOneOrNullResult()
       ;
   }
   */
}

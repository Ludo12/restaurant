<?php

namespace App\Repository;

use App\Entity\PlaceResto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlaceResto|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceResto|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceResto[]    findAll()
 * @method PlaceResto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRestoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaceResto::class);
    }

    public function all_elements()
    {
        return $this->createQueryBuilder('r')
            ->orderBy('r.date_reserv', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findByTotalByDate($value, $service)
    {
        return $this->createQueryBuilder('r')
            ->join('r.planning', 'ho', 'WITH', 'ho.service =:ser')
            ->setParameter('ser', $service)
            ->andWhere('r.date_reserv =:val')
            ->setParameter('val', $value)
            ->select('SUM(r.placeReserv) as plreserve')
            ->getQuery()
            ->getResult();
    }

    public function sumByDate()
    {
        return $this->createQueryBuilder('r')
            ->select("DISTINCT DATE_FORMAT(r.date_reserv, '%Y-%m-%d') as date")
            ->addSelect('SUM(r.placeReserv) as plreserve')
            ->groupBy('r.date_reserv')
            ->orderBy('date','DESC')
            ->getQuery()
            ->getResult();
    }

    public function verif_existe($value, $client, $planning)
    {
        $qb = $this->createQueryBuilder('r')
            ->where("DATE_FORMAT(r.date_reserv, '%Y-%m-%d') =:val")
            ->setParameter('val', $value)
            ->andWhere('r.clients=:cli')
            ->setParameter('cli', $client)
            ->Join('r.planning', 'h', 'WITH', 'h.service =:pla')
            ->setParameter('pla', $planning)
            ->addSelect('h');

        return $qb->getQuery()->getResult();
    }

    /*
    public function findOneBySomeField($value): ?PlaceResto
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200623134939 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, phone VARCHAR(64) NOT NULL, email VARCHAR(255) NOT NULL, message LONGTEXT NOT NULL, reservation INT DEFAULT NULL, date DATETIME NOT NULL, INDEX IDX_4C62E63819EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE planning (id INT AUTO_INCREMENT NOT NULL, hour VARCHAR(64) NOT NULL, minute VARCHAR(64) NOT NULL, service VARCHAR(64) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E63819EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE place_resto ADD planning_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE place_resto ADD CONSTRAINT FK_20574E923D865311 FOREIGN KEY (planning_id) REFERENCES planning (id)');
        $this->addSql('CREATE INDEX IDX_20574E923D865311 ON place_resto (planning_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE place_resto DROP FOREIGN KEY FK_20574E923D865311');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE planning');
        $this->addSql('DROP INDEX IDX_20574E923D865311 ON place_resto');
        $this->addSql('ALTER TABLE place_resto DROP planning_id');
    }
}

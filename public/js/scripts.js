//on verifie si le DOM est actif
window.onload = () => {
    // On va chercher la div dans le HTML
    let calendarEl = document.getElementById('calendar');
    // On instancie le calendrier
    let calendar = new FullCalendar.Calendar(calendarEl, {
            // On charge les composants "dayGrid", "timeGrid","interaction"
            plugins: ['dayGrid', 'timeGrid', 'interaction'],
            //mettre la langue en français
            locale: 'fr',
            firstDay: 1,
            //Dans le header, 3 sections (left, center et right) qui permettent de définir ce que je souhaite afficher.
            //Les valeurs séparées par des virgules seront collées ensemble, les valeurs séparées par des espaces seront séparées.
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth'
            },
            //Cette ligne modifie uniquement l'affichage des dates
            buttonText: {
                today: "Aujourd'hui",
                month: 'Mois',
            },
            //permet de modifier les éléments des cellules de jour qui font partie de chaque vue du calendrier.
            dayRender: function (date) {
                let today = new Date();
                //si la date sélectionnée ('d-m-Y 00:00:00') est inférieure à la date du jour(date courante à la seconde prêt)
                //et si la date sélectionnée('d-m-Y') est différente de la date du jour('d-m-Y')
                if (date.date.valueOf() < today.valueOf() && date.date.toLocaleDateString() !== today.toLocaleDateString()) {
                    date.el.style.backgroundColor = 'silver';
                }
                //si la date égale lundi
                if (date.date.getDay() === 1) {
                    date.el.style.backgroundColor = 'red';
                }

            },
            //me permet de modifier des éléments en fonction du click sur le jour
            dateClick: function (info) {
                let today = new Date();
                //si la date est différente de lundi
                if (info.date.getDay() !== 1) {
                    //si la date sélectionnée ('d-m-Y 00:00:00') est supérieure à la date du jour(date courante à la seconde prêt)
                    //ou si la date sélectionnée('d-m-Y') est égale à la date du jour('d-m-Y')
                    if (info.date.valueOf() > today.valueOf() || info.date.toLocaleDateString() === today.toLocaleDateString()) {
                        let modal = document.getElementById("myModal");
                        let balises = document.querySelectorAll('a.js-res');
                        let mes_td = document.querySelectorAll('td.fc-day.fc-future');
                        let today_td = document.querySelector('td.fc-day.fc-today');

                        today_td.style.backgroundColor = '#fcf8e3';

                        for (let mon_td of mes_td) {
                            if (mon_td.style.backgroundColor !== 'red') {
                                mon_td.style.backgroundColor = '#DAF6CD';
                            }
                        }

                        let span = document.getElementsByClassName("close")[0];
                        modal.style.display = "block";

                        for (let balise of balises) {
                            if (balise.attributes.item(0).value === `btn btn-anis m-1 js-res soir` && info.date.getDay() === 0) {
                                balise.style.display = 'none';
                            } else {
                                balise.style.display = 'inline-block';
                            }
                            balise.onclick = function () {
                                document.getElementById('place_resto_date_reserv_time_hour').value = parseInt(balise.innerHTML.substr(0, 2));
                                document.getElementById('place_resto_date_reserv_time_minute').value = parseInt(balise.innerHTML.substr(balise.innerHTML.length - 2, 2));

                                modal.style.display = "none";
                            };
                        }
                        span.onclick = function () {
                            info.dayEl.backgroundColor = '#DAF6CD';
                            modal.style.display = "none";
                        };
                        window.onclick = function (event) {
                            if (event.target === modal) {
                                modal.style.display = "none";
                            }
                        };
                        document.getElementById('place_resto_date_reserv_date_day').value = info.date.getDate();
                        document.getElementById('place_resto_date_reserv_date_month').value = info.date.getMonth() + 1;
                        document.getElementById('place_resto_date_reserv_date_year').value = info.date.getFullYear();
                        // change la couleur de fond du jour
                        info.dayEl.style.backgroundColor = 'green';
                    }
                }
            }
        }
        )
    ;

    // On affiche le calendrier
    calendar.render();

};
